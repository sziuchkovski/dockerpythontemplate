FROM python:2.7.14-stretch

WORKDIR /usr/src/app

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt
RUN rm requirements.txt

COPY src/ .

CMD [ "python", "./main.py" ]
